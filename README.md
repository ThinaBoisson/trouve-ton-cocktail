# Trouve ton cocktail

## README du projet trouve-ton-cocktail (*Find your cocktail !*) de Pirathina Boisson.

*Find your cocktail !* est un projet full-stack développé en Java / SpringBoot et HTML / CSS / JavaScript pour l'affichage de cocktails et de recettes à partir de l'API externe *TheCocktailDB*. 
L’objectif du projet était de proposer une application web optimisée sur une approche MVC pour la manipulation des données (recherche, lecture).

Le projet est composé de deux parties décrites ci-dessous :

1.  Back-end

-   Langage : Java 1.8
-   Framework : SpringBoot 2.4.1
-   Design pattern : MVC (Modèle-Vue-Controller)
-   Librairies / Frameworks additionnel(le)s : JUnit 4.13.1
-   Outils autres : SonarLint, Postman

2.  Front-End

-   Langages : HTML5 / CSS3 ; JavaScript
-   Librairies / Frameworks : Bootstrap 4.5.2 ; jQuery
-   Outils autres : SonarLint

***********************************************************************************
***Instructions d’import du projet trouve-ton-cocktail de Pirathina Boisson.***
***********************************************************************************

1.  Import du back-end

Prérequis : Avoir un IDE sur son poste (Eclipse), jdk minimum : 1.8

-   Aller sur gitLab : [https://gitlab.com/ThinaJeya/trouve-ton-cocktail](https://gitlab.com/ThinaJeya/trouve-ton-cocktail) et télécharger le fichier archive (.zip) ou récupérer le clone du repository : [https://gitlab.com/ThinaJeya/trouve-ton-cocktail.git](https://gitlab.com/ThinaJeya/trouve-ton-cocktail.git)
-   Importer le projet dans votre IDE favori.
-   Exécuter le code (run).

2.  Import du front-end

Prérequis : Avoir un IDE sur son poste (Visual Studio Code)

- Ouvrir le répertoire "front" du projet dans votre IDE favori
- Démarrer le live serveur dans votre IDE (port 5500 par défaut ; en bas à droite dans Visual Studio Code)
- Aller sur la page d'accueil de l'application web :  [http://localhost:5500/page_accueil.html](http://localhost:5500/page_accueil.html)  (modifier le port d'accès si besoin)

**Vous pouvez naviguer sur l'application web une fois que le code back-end du projet aura été exécuté dans l'IDE.**

Note : Les requêtes Postman associées au projet sont également fournies dans le projet.