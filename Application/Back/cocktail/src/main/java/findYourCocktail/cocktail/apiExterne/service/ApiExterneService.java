package findYourCocktail.cocktail.apiExterne.service;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import findYourCocktail.cocktail.exception.DrinkImplException;

/**
 * Couche Service de ApiExterne
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface ApiExterneService {

	/**
	 * Méthode pour obtenir un array json depuis un objet json pour une recherche par lettre ou par nom
	 * @param parametre
	 * @return array json de la recherche par lettre ou par nom
	 * @throws IOException
	 * @throws DrinkImplException 
	 */
	public JSONArray rechercheDrinkParLettreOuNom(String parametre) throws IOException, DrinkImplException;
	
	/**
	 * Permet d'obtenir l'objet json résultant d'une requete http avec un paramètre de type string
	 * @param url
	 * @param parametre
	 * @return objet json résultant d'une requete http avec un paramètre de type string
	 * @throws IOException
	 * @throws DrinkImplException 
	 */
	public JSONObject requeteHttpJsonObjectStringParametre(String url, String parametre) throws IOException, DrinkImplException;
	
	/**
	 * Méthode pour obtenir un array json depuis un objet json pour une recherche par id
	 * @param url
	 * @param parametre
	 * @return array json de la recherche par id
	 * @throws DrinkImplException
	 * @throws IOException
	 */
	public JSONArray rechercheDrinkParId(String url, int parametre) throws DrinkImplException, IOException;
	
	/**
	 * Permet d'obtenir l'objet json résultant d'une requete http avec un paramètre de type integer
	 * @param url
	 * @param parametre
	 * @return objet json résultant d'une requete http avec un paramètre de type integer
	 * @throws IOException
	 * @throws DrinkImplException 
	 */
	public JSONObject requeteHttpJsonObjectIntegerParametre(String url, int parametre) throws DrinkImplException, IOException;
	
	/**
	 * Méthode pour obtenir un array json d'un drink aléatoire 
	 * @param url
	 * @return array json d'un drink aléatoire 
	 * @throws IOException
	 * @throws DrinkImplException 
	 */
	public JSONArray rechercheDrinkRandom(String url) throws IOException, DrinkImplException;
}
