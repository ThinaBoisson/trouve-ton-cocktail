package findYourCocktail.cocktail.drink.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import findYourCocktail.cocktail.drink.bean.Drink;
import findYourCocktail.cocktail.exception.DrinkImplException;

/**
 * Couche Service de Drink
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface DrinkService {
	
	/**
	 * Service pour afficher tous les drinks
	 * @return liste de tous les drinks
	 * @throws Exception
	 */
	public List<Drink> afficherTousLesDrinks() throws Exception;

	/**
	 * Service pour afficher les drinks selon la lettre entrée en paramètre
	 * @param lettre
	 * @return liste des drinks selon la lettre entrée en paramètre
	 * @throws Exception
	 */
	public List<Drink> afficherDrinksParPremiereLettre(String lettre) throws Exception;

	/**
	 * Service pour afficher les drinks selon le nom entré en paramètre
	 * @param nom
	 * @return liste des drinks selon le nom entré en paramètre
	 * @throws Exception
	 */
	public List<Drink> afficherDrinkParNom(String nom) throws Exception;

	/**
	 * Service pour afficher le drink associé à l'id entré en paramètre
	 * @param id
	 * @return drink associé à l'id entré en paramètre
	 * @throws Exception
	 */
	public List<Drink> afficherDrinkParId(int id) throws Exception;

	/**
	 * Service pour afficher un drink de facon aléatoire
	 * @return drink aléatoire
	 * @throws Exception
	 */
	public List<Drink> afficherUnDrinkRandom() throws Exception;

	
	/**
	 * Service pour afficher les drinks associés à la catégorie entrée en paramètre
	 * @param categorie
	 * @return liste des drinks selon la catégorie entré en paramètre
	 * @throws IOException
	 * @throws DrinkImplException
	 */
	public List<Drink> afficherDrinksParCategorie(String categorie) throws IOException, DrinkImplException;

}
