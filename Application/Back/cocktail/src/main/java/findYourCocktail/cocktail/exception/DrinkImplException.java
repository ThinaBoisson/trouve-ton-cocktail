package findYourCocktail.cocktail.exception;

@SuppressWarnings("serial")
public class DrinkImplException extends Exception {

	public enum MessageDrinkException {
		LISTERETOURNEEVIDE("La liste retournée pour cette requête est vide ou nulle"),
		PARAMETREINCORRECT("Le paramètre est incorrect"),
		PARAMETREMETHODEINTERNEINCORRECT("Le paramètre entré dans la méthode interne est incorrect"),
		REPONSEREQUETENULL("La réponse de la requête http vaut null");
		
		private String message;
		
		private MessageDrinkException(String message) {
			this.message = message;
		}
		
		public String getMessage() {
			return message;
		}
	}
	
	public DrinkImplException(String messageException) {
		super(messageException);
	}	
}