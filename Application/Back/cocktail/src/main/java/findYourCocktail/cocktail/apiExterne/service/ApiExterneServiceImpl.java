package findYourCocktail.cocktail.apiExterne.service;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import findYourCocktail.cocktail.exception.DrinkImplException;
import findYourCocktail.cocktail.exception.DrinkImplException.MessageDrinkException;

/**
 * Couche Implementation de ApiExterneService
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class ApiExterneServiceImpl implements ApiExterneService {

	private static final String KEY_DRINK = "drinks";

	private static final String URL_DRINKS_PAR_LETTRE = "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=";
	private static final String URL_DRINKS_PAR_NOM = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=";
	private static final String URL_DRINKS_PAR_ID = "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=";

	@Override
	public JSONArray rechercheDrinkParLettreOuNom(String parametre) throws IOException, DrinkImplException {
		JSONArray jsonArray = new JSONArray();

		if(!StringUtils.isBlank(parametre) && parametre.length() == 1) {
			JSONObject data = requeteHttpJsonObjectStringParametre(URL_DRINKS_PAR_LETTRE, parametre);

			if(!data.isNull(KEY_DRINK)) {
				jsonArray = data.getJSONArray(KEY_DRINK);
			}

		} else if(!StringUtils.isBlank(parametre) && parametre.length() > 1) {
			JSONObject data = requeteHttpJsonObjectStringParametre(URL_DRINKS_PAR_NOM, parametre);

			if(!data.isNull(KEY_DRINK)) {
				jsonArray = data.getJSONArray(KEY_DRINK);
			} 
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
		}
		return jsonArray;
	}

	@Override
	public JSONObject requeteHttpJsonObjectStringParametre(String url, String parametre) throws IOException, DrinkImplException {
		OkHttpClient httpClient = new OkHttpClient();
		JSONObject data = null;

		if(!StringUtils.isAllBlank(url, parametre)) {
			Request request = new Request.Builder()
					.url(url + parametre)
					.get()
					.build();

			Response response = httpClient.newCall(request).execute();

			if(response != null) {
				String bodyResponse = response.body().string();
				data = new JSONObject(bodyResponse);
			} else {
				throw new DrinkImplException(MessageDrinkException.REPONSEREQUETENULL.getMessage());
			}
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
		}
		return data;
	}

	@Override
	public JSONArray rechercheDrinkParId(String url, int parametre) throws DrinkImplException, IOException {
		JSONArray jsonArray = new JSONArray();

		if(!StringUtils.isBlank(url) && parametre > 0) {
			JSONObject data = requeteHttpJsonObjectIntegerParametre(URL_DRINKS_PAR_ID, parametre);

			if(!data.isNull(KEY_DRINK)) {
				jsonArray = data.getJSONArray(KEY_DRINK);
			} 
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
		}
		return jsonArray;
	}

	@Override
	public JSONObject requeteHttpJsonObjectIntegerParametre(String url, int parametre) throws DrinkImplException, IOException {
		OkHttpClient httpClient = new OkHttpClient();
		JSONObject data = null;

		Request request = new Request.Builder()
				.url(url + parametre)
				.get()
				.build();
		Response response = httpClient.newCall(request).execute();

		if(response != null) {
			String bodyResponse = response.body().string();
			data = new JSONObject(bodyResponse);	
		} else {
			throw new DrinkImplException(MessageDrinkException.REPONSEREQUETENULL.getMessage());
		}

		return data;
	}

	@Override
	public JSONArray rechercheDrinkRandom(String url) throws IOException, DrinkImplException {
		JSONArray jsonArray = new JSONArray();

		OkHttpClient httpClient = new OkHttpClient();
		JSONObject data = null;

		if(!StringUtils.isBlank(url)) {
			Request request = new Request.Builder()
					.url(url)
					.get()
					.build();
			Response response = httpClient.newCall(request).execute();

			if(response != null) {
				String bodyResponse = response.body().string();
				data = new JSONObject(bodyResponse);

				if(!data.isNull(KEY_DRINK)) {
					jsonArray = data.getJSONArray(KEY_DRINK);
				}				
			} else {
				throw new DrinkImplException(MessageDrinkException.REPONSEREQUETENULL.getMessage());
			}
		} else {
			throw new DrinkImplException(MessageDrinkException.PARAMETREMETHODEINTERNEINCORRECT.getMessage());
		}
		return jsonArray;
	}
}
