document.addEventListener('DOMContentLoaded', () => {
  ajaxRequestDrinkCocktailPage();
  ajaxRequestRandomCocktailDisplay("#cocktail-div-9");
  ajaxRequestRandomCocktailDisplay("#cocktail-div-10");
  ajaxRequestRandomCocktailDisplay("#cocktail-div-11");
  ajaxRequestRandomCocktailDisplay("#cocktail-div-12");
});

/**
 * Array des noms des attributs contenant le nom des ingrédients
 */
const ARRAYHEADERSINGREDIENTSNAME = ["strIngredient1", "strIngredient2", "strIngredient3", "strIngredient4", "strIngredient5", "strIngredient6", "strIngredient7", "strIngredient8", "strIngredient9", "strIngredient10", "strIngredient11", "strIngredient12", "strIngredient13", "strIngredient14", "strIngredient15"];

/**
 * Array des noms des attributs contenant le dosage des ingrédients
 */
const ARRAYHEADERSINGREDIENTSMEASURE = ["strMeasure1", "strMeasure2", "strMeasure3", "strMeasure4", "strMeasure5", "strMeasure6", "strMeasure7", "strMeasure8", "strMeasure9", "strMeasure10", "strMeasure11", "strMeasure12", "strMeasure13", "strMeasure14", "strMeasure15"];

/**
 * Array contenant le nom des verres et le lien vers le logo associé pour l'affichage
 */
const ARRAYGLASSESLINKS = [['HIGHBALL GLASS', "./logo/logos-glasses/highball-glass.svg"], ['COCKTAIL GLASS', "./logo/logos-glasses/cocktail-glass.svg"],  ['OLD-FASHIONED GLASS', "./logo/logos-glasses/old-fashioned-glass.png"], 
['WHISKEY GLASS', "./logo/logos-glasses/old-fashioned-glass.png"], ['COLLINS GLASS', "./logo/logos-glasses/highball-glass.svg"], ['POUSSE CAFE GLASS', "./logo/logos-glasses/cordial-glass.svg"], 
['CHAMPAGNE FLUTE', "./logo/logos-glasses/champagne-flute.svg"], ["WHISKEY SOUR GLASS", "./logo/logos-glasses/old-fashioned-glass.png"],  ["CORDIAL GLASS", "./logo/logos-glasses/cordial-glass.svg"], 
["BRANDY SNIFTER", "./logo/logos-glasses/snifter-glass.svg"], ["WHITE WINE GLASS", "./logo/logos-glasses/wine-glass.svg"], ["NICK AND NORA GLASS", "./logo/logos-glasses/nick-and-nora-glass.svg"], 
["HURRICANE GLASS", "./logo/logos-glasses/hurricane-glass.svg"], ["COFFEE MUG", "./logo/logos-glasses/coffee-mug.svg"], ["SHOT GLASS", "./logo/logos-glasses/shot-glass.svg"], 
["JAR", "./logo/logos-glasses/jar-glass.svg"], ["IRISH COFFEE CUP", "./logo/logos-glasses/irish-coffee-glass.svg"], ["PUNCH BOWL", "./logo/logos-glasses/punch-bowl.svg"], 
["PITCHER", "./logo/logos-glasses/pitcher-glass.svg"], ["PINT GLASS", "./logo/logos-glasses/pint-glass.svg"], ["COPPER MUG", "./logo/logos-glasses/mule-mug.svg"], 
["WINE GLASS", "./logo/logos-glasses/wine-glass.svg"], ["BEER MUG", "./logo/logos-glasses/beer-mug.svg"], ["MARGARITA/COUPETTE GLASS", "./logo/logos-glasses/margarita-glass.svg"], 
["BEER PILSNER", "./logo/logos-glasses/beer-mug.svg"], ["BEER GLASS", "./logo/logos-glasses/beer-mug.svg"], ["PARFAIT GLASS", "./logo/logos-glasses/parfait-glass.svg"], 
["MASON JAR", "./logo/logos-glasses/jar-glass.svg"], ["MARGARITA GLASS", "./logo/logos-glasses/margarita-glass.svg"], ["MARTINI GLASS", "./logo/logos-glasses/martini-glass.svg"], 
["BALLOON GLASS", "./logo/logos-glasses/brandy-glass.svg"], ["COUPE GLASS", "./logo/logos-glasses/coupe-glass.svg"], ["", "./logo/logos-glasses/cocktail-glass.svg"]];


//Affichage du cocktail principal de la page

/**
 * Requête ajax pour l'affichage du cocktail principal de la page
 */
function ajaxRequestDrinkCocktailPage() {
  const ERRORMESSAGE = "Erreur dans la requête AJAX !";
  let idDrink = getParameterFromUrl();

  $.ajax({
    url: 'http://localhost:8081/drinks/drinkParId/' + idDrink,
    type: 'GET',
    dataType: 'JSON',
    contentType: "application/json",
    success: function (data, status) {
      if (data !== null || data !== undefined) {
        for (let i = 0; i < data.length; i++) {
          DrinkGlassSearch(data[i]);
          displayTitleImage(data[i]);
          displayListOfIngredientText(data[i]);
          displayInstructions(data[i]);
          displayIngredientsForShoppingList(data[i]);
        }
      }
    },
    error: function (erreur) {
      console.log(ERRORMESSAGE + erreur.status);
    }
  });
}

/**
 * Permet de récupérer le contenu du paramètre id de l'url
 */
function getParameterFromUrl() {
  let url_string = window.location.href;
  let url = new URL(url_string);
  let idParameter = url.searchParams.get("id");
  return idParameter;
}

/**
 * Permet d'afficher le nom et l'image du cocktail
 * @param {JSON} data 
 */
function displayTitleImage(data) {
  let nomCocktail = document.querySelector("#nom-cocktail");
  let imgCocktail = document.querySelector("#img-cocktail");

  $(nomCocktail).append(data.strDrink);
  $(imgCocktail).append(`<img src="${data.strDrinkThumb}" alt="${data.strDrink} picture"></img>`);
}

/**
 * Permet d'afficher la liste des ingrédients et leurs dosages
 * @param {JSON} data 
 */
function displayListOfIngredientText(data) {
  let ulListeIngredient = document.getElementById('liste-ingredients-dosage');
  for (let j = 0; j < ARRAYHEADERSINGREDIENTSNAME.length; j++) {
    for (let strIngredient in data) {
      if (ARRAYHEADERSINGREDIENTSNAME[j] === strIngredient) {
        if (data[strIngredient] !== "null" && data[strIngredient] !== "") {
          $(ulListeIngredient).append(`<li>- ${data[strIngredient]} : ${displayIngredientMeasure(data)[j]}</li>`);
        }
      }
    }
  }
}

/**
 * Récupère les dosages des ingrédients dans un array en comparant les entêtes json avec le ARRAYHEADERSINGREDIENTSMEASURE
 * @param {JSON} data 
 */
function displayIngredientMeasure(data) {
  let result = [];

  for (let j = 0; j < ARRAYHEADERSINGREDIENTSMEASURE.length; j++) {
    for (let strMeasure in data) {
      if (ARRAYHEADERSINGREDIENTSMEASURE[j] === strMeasure) {
        if (data[strMeasure] !== "null" && data[strMeasure] !== "") {
          result.push(data[strMeasure]);
        } else if (data[strMeasure] === "null") {
          result.push("sufficient quantity for");
        }
      }
    }
  }
  return result;
}

/**
 * Affichage des instructions de la recette (avec retour à la ligne pour chaque étape)
 * @param {JSON} data 
 */
function displayInstructions(data) {
  let instructions = data.strInstructions;
  let regex = /\.\s?(?![^\(]*\))/;
  let splitInstructions = instructions.split(regex);
  let descriptionRecette = document.getElementById('description-recette');

  $(descriptionRecette).append(`<h4>How to prepare ${data.strDrink}?</h4>`);

  for (let i = 0; i < splitInstructions.length; i++) {
    if (splitInstructions[i] !== "") {
      $(descriptionRecette).append(`<p>${splitInstructions[i]}.</p>`);
    }
  }
}

/**
 * Affichage de la shopping list du cocktail principal avec les images des ingrédients
 * @param {JSON} data 
 */
function displayIngredientsForShoppingList(data) {
  let divParent = document.getElementById('liste-courses');

  for (let j = 0; j < ARRAYHEADERSINGREDIENTSNAME.length; j++) {
    for (let strIngredient in data) {
      if (ARRAYHEADERSINGREDIENTSNAME[j] === strIngredient && data[strIngredient] !== "null" && data[strIngredient] !== "") {
        $(divParent).append(
          `<div class="bloc-ingredient col-md-3 col-sm-6" id="bloc-ingredient-${j.toString()}">
              <img src="https://www.thecocktaildb.com/images/ingredients/${data[strIngredient]}-Medium.png" alt="${data[strIngredient]} picture">
              <h5>${data[strIngredient]}</h5>
            </div>`);
      }
    }
  }
}

/**
 * Recherche du verre associé au cocktail et du lien vers le logo
 * @param {JSON} data 
 */
function DrinkGlassSearch(data) {
  let nomGlass = data.strGlass.toUpperCase();

  for(let i = 0; i < ARRAYGLASSESLINKS.length; i++){
    for(let j = 0; j < ARRAYGLASSESLINKS[i].length; j = j+2){
      if(nomGlass !== "null" && nomGlass === ARRAYGLASSESLINKS[i][j]){
        displayDrinkGlass(nomGlass, ARRAYGLASSESLINKS[i][j+1]);
        break;
      }
    }
  }
}

/**
 * Affichage du verre adapté pour le cocktail
 * @param {string} nomGlass 
 * @param {string} lienLogo 
 */
function displayDrinkGlass(nomGlass, lienLogo) {
  let divImgGlass = document.getElementById('logo-type-verre');

  $(divImgGlass).append(`
  <h6>${nomGlass}</h6>
  <img src=${lienLogo} alt="Logo for cocktail glass">`);
}

/**
 * Requête ajax pour récupérer des cocktails suggestions
 * @param {string} htmlIdDiv 
 */
function ajaxRequestRandomCocktailDisplay(htmlIdDiv) {
  $.ajax({
    url: 'http://localhost:8081/drinks/drinkRandom',
    type: 'GET',
    dataType: 'JSON',
    contentType: "application/json",
    success: function (data, status) {
      displayDrinkHTML(data, htmlIdDiv);
    },
    error: function (erreur) {
      console.log(ERRORMESSAGE + erreur.status);
    }
  });
}

/**
 * Affichage du cocktail avec son titre et son image
 * @param {JSON} data 
 * @param {string} htmlIdDiv 
 */
function displayDrinkHTML(data, htmlIdDiv) {
  if (data !== null || data !== undefined) {
    for (let i = 0; i < data.length; i++) {
      $(htmlIdDiv).append(
        `<a class="item-over" id="${data[i].idDrink}" onclick="afficherCocktail(this.id)">
                  <div class="cocktail-picture" id="${data[i].idDrink}">
                      <img class="drink-picture" src="${data[i].strDrinkThumb}" id="random-cocktail-picture-${data[i].idDrink}">
                  </div>
                  <h3 class="cocktail-title" id="random-cocktail-title-${data[i].idDrink}">${data[i].strDrink}</h3>
              </a>`);
    }
  }
}

/**
 * chargement de la page cocktail avec en paramètre d'url l'id du cocktail
 * @param {number} id 
 */
function afficherCocktail(id) {
  window.location.href = "./page_cocktail.html?id=" + id;
}